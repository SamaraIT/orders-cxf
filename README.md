# Spring Boot + CXF (JAX-RS) + XML Signature

`XAdES` extends the `XMLDSIG` specification, it is a standard that is designed and used to sign XML documents.  
Functionally, it has much in common with PKCS#7 but is more extensible and focused on signing XML documents.  

There are three Signature Types:   
- Detached (the signature don't includes the file signed)
- Enveloped (when the signature comprises a part of the document containing the signed data) 
- Enveloping (when the signature contains the signed data withing itself).

### Links
- http://localhost:8080/services/orders/11
```xml
<ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
    <ds:SignedInfo>
        <ds:CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"/>
        <ds:SignatureMethod Algorithm="http://www.w3.org/2000/09/xmldsig#rsa-sha1"/>
        <ds:Reference URI="#_8e80d10d-61e0-4fad-baa8-e46d0e889c6e">
            <ds:Transforms>
                <ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#"/>
            </ds:Transforms>
            <ds:DigestMethod Algorithm="http://www.w3.org/2000/09/xmldsig#sha1"/>
            <ds:DigestValue>r1z4Twx+cbyDU179cqk5cywUg1c=</ds:DigestValue>
        </ds:Reference>
    </ds:SignedInfo>
    <ds:SignatureValue>qItGJF9+a9r/fEyWN87rxOBy7NqzvdNEevfU03696f2qRsqAIx0sP1xZ3bAXtIODOh52fRO6hTAdlMghgs7bV0ouQgyUfnyJ3646tdY2LDF4XCGEa6S0hGD/vs4uDnycIAapSKr1Nbrb9tXIjGHB4bRMWDIcCe8PKrRDKDtHpBTxx6R1ULfLWRUxjh7eAFCc4XluU4fLWd7JajCd3xtNFPZkJAQtf8skRQuYRUlBFgDqpG701YaNZCduMgu486wcmTs5Txbwh9r/yZV6UAiJaBlH2KZonDnZJRsesa2PPRyJMpJ9uXIMIOOOn54Vt0w4nND/KEDh2E5oPewmg5NkLg==</ds:SignatureValue>
    <ds:KeyInfo xmlns:ds="http://www.w3.org/2000/09/xmldsig#">
        <ds:X509Data>
            <ds:X509Certificate>MIICxzCCAa+gAwIBAgIEOZzjCjANBgkqhkiG9w0BAQUFADAUMRIwEAYDVQQDEwlsb2NhbGhvc3QwHhcNMjAwNDEwMTkxNjU4WhcNMjQwNDEwMTkxNjU4WjAUMRIwEAYDVQQDEwlsb2NhbGhvc3QwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCopoHb4qE2/DHwmycDYOWxWcCO7De47K6bHCJBEEaALgQpH6DphAgMIiG1R5b2JPXS/zQ1f+Y3gcTdqdVopCA+HXIda8GoQMsb14uSd43h5zMAuz1GlukVlxS3FepevIZMvp+Rjk0aY7yqfSAx6w9R2udNPcwCTnRtkmoxSzXS11wUg2WBxYQXezUPndqcfUcGzgMU8awRc3gy2sXREMZ6uL15/NlAofx0p2AR1/QA8xcxCG+0ruS1MMQDDjs9WilYUCXiM/+ToMInFrda15mqxTHe2PXRx2qe6F2ckbod1qE+X4LDnJIx3OzYMyOx9MXJJIUHvp4HWVgLlgN6xZDvAgMBAAGjITAfMB0GA1UdDgQWBBQJIzETQUnv26ef51DIMkGWlg3mkjANBgkqhkiG9w0BAQUFAAOCAQEAcWPJYcVDUSPqwkCh9nYiCGuKJLzSV2vdc1rQKkU3uCPrxqNhn1UoHm9Kyowq2QiM3+QobZc8ZVCTk2acYleyMPAJFnA/rcv3p4JCBiAVaMyBPIYZ64/tBNvQeSqX6oGHFvazRE/4ThMVAF/ySY+0qKQ4dO8WjN0HpRanos+KHrlhAmz402t8VqblS7HrbbQkFqkN0L6DU/P5wMJcjxxWWI7GFAatzVyYpRC2cj2abhwF4BemUg5T3IQ27uVA4tRWKAS6x5h1CIaH/Ot98g6tLDcmfnDtBS1aEN2LG67kmIMwImvRNVPZPnR+hzvjptIFGrzKTzl9kmlMmQldsS4pbg==</ds:X509Certificate>
        </ds:X509Data>
        <ds:KeyValue>
            <ds:RSAKeyValue>
                <ds:Modulus>qKaB2+KhNvwx8JsnA2DlsVnAjuw3uOyumxwiQRBGgC4EKR+g6YQIDCIhtUeW9iT10v80NX/mN4HE3anVaKQgPh1yHWvBqEDLG9eLkneN4eczALs9RpbpFZcUtxXqXryGTL6fkY5NGmO8qn0gMesPUdrnTT3MAk50bZJqMUs10tdcFINlgcWEF3s1D53anH1HBs4DFPGsEXN4MtrF0RDGeri9efzZQKH8dKdgEdf0APMXMQhvtK7ktTDEAw47PVopWFAl4jP/k6DCJxa3WteZqsUx3tj10cdqnuhdnJG6HdahPl+Cw5ySMdzs2DMjsfTFySSFB76eB1lYC5YDesWQ7w==</ds:Modulus>
                <ds:Exponent>AQAB</ds:Exponent>
            </ds:RSAKeyValue>
        </ds:KeyValue>
    </ds:KeyInfo>
    <ds:Object>
        <OrderResponse Id="_8e80d10d-61e0-4fad-baa8-e46d0e889c6e">
            <id>9f7dfa80-797f-41b7-9577-b12c7ad1fc30</id>
            <created/>
            <email>igor@samarait.hr</email>
            <items>
                <amount>10.11</amount>
                <name>Lizalica</name>
            </items>
            <items>
                <amount>4.16</amount>
                <name>Bombon</name>
            </items>
            <price>
                <amount>10</amount>
                <currency>EUR</currency>
            </price>
        </OrderResponse>
    </ds:Object>
</ds:Signature>
```
- http://localhost:8080/actuator/health
```json
{
    "status": "UP"
}
```

- http://localhost:8080/services/openapi.json
- http://localhost:8080/services/openapi.yaml
- [Swagger UI](http://localhost:8080/services/demo/api-docs?url=/services/demo/openapi.json)

## CXF
https://cxf.apache.org/docs/jax-rs.html

### XML Security
https://cxf.apache.org/docs/jax-rs-xml-security.html


### with Spring Boot
https://cxf.apache.org/docs/springboot.html#SpringBoot-SpringBootCXFJAX-RSStarter  
https://github.com/apache/cxf/tree/master/distribution/src/main/release/samples/jaxws_spring_boot  
https://www.baeldung.com/apache-cxf-rest-api  
https://tech.asimio.net/2017/06/12/Implementing-APIs-using-Spring-Boot-CXF-and-Swagger.html  

### XML
- http://blog.bdoughan.com/2012/02/jaxb-and-package-level-xmladapters.html
- https://blog.sebastian-daschner.com/entries/jaxrs-convert-params
- https://cxf.apache.org/docs/jaxrs-services-configuration.html

#### LocalDateTime as request parameter

Not working:  
```java
@XmlJavaTypeAdapters({
        @XmlJavaTypeAdapter(type = LocalDateTime.class, value = LocalDateTimeAdapter.class)
})
package hr.samarait.demo.orders.web.jaxb;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;
import java.time.LocalDateTime;
```

working:
```java
endpoint.setProvider(new LocalDateTimeConverterProvider());
```

### JAX-RS
https://mkyong.com/webservices/jax-rs/jax-rs-queryparam-example/  

### with Spring
https://www.appdirect.com/blog/how-to-easily-build-rest-web-services-with-java-spring-and-apache-cxf  
https://www.learninjava.com/restful-webservice-using-cxf/  

### OpenAPI feature
- http://localhost:8080/services -> does NOT work ?!
- https://cxf.apache.org/docs/swagger2feature.html

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.3.5.RELEASE/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.3.5.RELEASE/maven-plugin/reference/html/#build-image)
* [Spring Boot Actuator](https://docs.spring.io/spring-boot/docs/2.3.5.RELEASE/reference/htmlsingle/#production-ready)
* [Spring Boot DevTools](https://docs.spring.io/spring-boot/docs/2.3.5.RELEASE/reference/htmlsingle/#using-boot-devtools)
