package hr.samarait.demo.orders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrdersCxfApplication {

  public static void main(String[] args) {
    SpringApplication.run(OrdersCxfApplication.class, args);
  }

}
