package hr.samarait.demo.orders.web;

import hr.samarait.demo.orders.model.OrderRequest;
import hr.samarait.demo.orders.model.OrderResponse;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.time.LocalDateTime;
import java.util.List;

@Path("/orders")
@Produces(MediaType.TEXT_XML)
// TODO check
//@Service
public interface OrdersWs {

  @POST
  OrderResponse createOrder(OrderRequest request);

  @GET
  List<OrderResponse> fetchOrders(@QueryParam("startDate") LocalDateTime startDate,
                                  @QueryParam("endDate") LocalDateTime endDate);

  @GET
  @Path("{id}")
  OrderResponse fetchOrder(@PathParam("id") String id);
}
