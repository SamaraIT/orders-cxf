package hr.samarait.demo.orders.web.jaxb;

import hr.samarait.demo.orders.util.Constants;

import javax.ws.rs.ext.ParamConverter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LocalDateTimeConverter implements ParamConverter<LocalDateTime> {

    private DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern(Constants.ISO_DATE_TIME);

    @Override
    public LocalDateTime fromString(String value) {
        if (value == null)
            return null;
        return LocalDateTime.parse(value, dateFormat);
    }

    @Override
    public String toString(LocalDateTime value) {
        if (value == null)
            return null;
        return value.format(dateFormat);
    }
}
