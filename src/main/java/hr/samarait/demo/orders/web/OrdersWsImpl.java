package hr.samarait.demo.orders.web;

import hr.samarait.demo.orders.model.OrderRequest;
import hr.samarait.demo.orders.model.OrderResponse;
import hr.samarait.demo.orders.service.OrdersService;

import java.time.LocalDateTime;
import java.util.List;

public class OrdersWsImpl implements OrdersWs {

  private final OrdersService ordersService;

  public OrdersWsImpl(OrdersService ordersService) {
    this.ordersService = ordersService;
  }

  @Override
  public OrderResponse createOrder(OrderRequest request) {
    return ordersService.createOrder(request);
  }

  // TODO xml signature is not working -> DEBUG org.apache.cxf.rs.security.xml.AbstractXmlSecOutInterceptor
  // body is not instanceof Document, not DOMSource, nor it can find JAXBElementProvider -> returns
  @Override
  public List<OrderResponse> fetchOrders(LocalDateTime startDate, LocalDateTime endDate) {
    List<OrderResponse> orders = ordersService.fetchOrders(startDate, endDate);
//    GenericEntity entity = new GenericEntity<List<OrderResponse>>(orders) {
//    };
//    return Response.ok(entity).build();
    return orders;
  }

  @Override
  public OrderResponse fetchOrder(String id) {
    return ordersService.fetchOrder(id);
  }
}
