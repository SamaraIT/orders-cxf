package hr.samarait.demo.orders.model;

import hr.samarait.demo.orders.web.jaxb.LocalDateTimeAdapter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@XmlRootElement(name = "OrderResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrderResponse {

    private String id;

    // for client - test - unmarshaller - ClientSpringIT
    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    // moved adapter to package-info - no package scanning so not needed
    private LocalDateTime created;
    private String email;

    private List<ItemDto> items = new ArrayList<>();

    private Amount price;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ItemDto {
        private String name;
        private BigDecimal amount;
    }

}
