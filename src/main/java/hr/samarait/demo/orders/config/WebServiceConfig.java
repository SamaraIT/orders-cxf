package hr.samarait.demo.orders.config;

import hr.samarait.demo.orders.service.OrdersService;
import hr.samarait.demo.orders.web.OrdersWsImpl;
import hr.samarait.demo.orders.web.jaxb.LocalDateTimeConverterProvider;
import lombok.extern.slf4j.Slf4j;
import org.apache.cxf.Bus;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.ext.logging.LoggingFeature;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.openapi.OpenApiFeature;
import org.apache.cxf.jaxrs.swagger.ui.SwaggerUiConfig;
import org.apache.cxf.rs.security.xml.XmlSigOutInterceptor;
import org.apache.cxf.rt.security.SecurityConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.security.auth.callback.CallbackHandler;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Configuration
@Slf4j
public class WebServiceConfig {

  @Autowired
  private Bus bus;

  @Bean
  public Server rsServer() {
    JAXRSServerFactoryBean server = new JAXRSServerFactoryBean();
    server.setBus(bus);
    server.setServiceBeans(Arrays.asList(new OrdersWsImpl(new OrdersService())));
    server.setAddress("/");
    server.setFeatures(Arrays.asList(openApiFeature(), loggingFeature()));

    // TODO
    server.setProviders(Arrays.asList(new LocalDateTimeConverterProvider()
      // Required for validating the in signature and removing it from the payload.
      //       It also persists the signature on the current Message which can be disabled.
      //, new XmlSigInHandler()
    ));

    //  add the interceptor which will add a signature to the outbound payload
    server.getOutInterceptors().add(xmlSigOutInterceptor());

    Map<String, Object> props = new HashMap<>();
    props.put(SecurityConstants.CALLBACK_HANDLER, servicePasswordCallback());
    props.put(SecurityConstants.SIGNATURE_PROPERTIES, "serviceKeystore.properties");
    log.debug(" *** rsServer properties:");
    props.forEach((k, v) -> log.debug(k + "=" + v));
    server.setProperties(props);

    log.info("** RS Server configured");
    return server.create();
  }

  @Bean
  public XmlSigOutInterceptor xmlSigOutInterceptor() {
    // SignatureProperties signatureProperties = new SignatureProperties();
    XmlSigOutInterceptor sigInterceptor = new XmlSigOutInterceptor();
    sigInterceptor.setStyle("enveloped");
    return sigInterceptor;
  }

  @Bean
  public CallbackHandler servicePasswordCallback() {
    return new ServiceKeystorePasswordCallback();
  }

  @Bean
  public OpenApiFeature openApiFeature() {
    final OpenApiFeature openApiFeature = new OpenApiFeature();
    openApiFeature.setPrettyPrint(true);
    openApiFeature.setTitle("Spring Boot CXF REST Application");
    openApiFeature.setContactName("Samara");
    openApiFeature.setDescription("This sample project demonstrates how to use CXF JAX-RS services"
      + " with Spring Boot. This demo has two JAX-RS class resources being"
      + " deployed in a single JAX-RS endpoint.");
    openApiFeature.setVersion("1.0.0");
    openApiFeature.setSwaggerUiConfig(new SwaggerUiConfig().url("/services/demo/openapi.json"));
    return openApiFeature;
  }

  private LoggingFeature loggingFeature() {
    LoggingFeature loggingFeature = new LoggingFeature();
    loggingFeature.setPrettyLogging(true);
    return loggingFeature;
  }
}
