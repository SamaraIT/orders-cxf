package hr.samarait.demo.orders.config;

import org.apache.wss4j.common.ext.WSPasswordCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ServiceKeystorePasswordCallback implements CallbackHandler {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());
  private Map<String, String> passwords = new HashMap<>();

  public ServiceKeystorePasswordCallback() {
    // alias, keypass
    passwords.put("myservicekey", "skpass");
  }

  @Override
  public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
    for (int i = 0; i < callbacks.length; i++) {
      WSPasswordCallback pc = (WSPasswordCallback) callbacks[i];

      String pass = passwords.get(pc.getIdentifier());
      if (pass != null) {
        pc.setPassword(pass);
        logger.debug("identifier: " + pc.getIdentifier() + ", usage: " + pc.getUsage());
        return;
      }
    }
  }
}
