package hr.samarait.demo.orders.service;

import hr.samarait.demo.orders.model.Amount;
import hr.samarait.demo.orders.model.OrderRequest;
import hr.samarait.demo.orders.model.OrderResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * in memory
 */
@Service
@Slf4j
public class OrdersService {

    private final List<OrderResponse> orders = new ArrayList<>();

    public OrdersService() {
        List<OrderRequest.ItemDto> items = new ArrayList<>();
        BigDecimal amount = BigDecimal.ZERO;
        List<String> names = new ArrayList<>();
        List<BigDecimal> amounts = new ArrayList<>();
        OrderRequest.ItemDto itemDto = new OrderRequest.ItemDto();
        itemDto.setProductId("33");
        itemDto.setQuantity(1);
        BigDecimal itemAmount = new BigDecimal("11.33").multiply(BigDecimal.valueOf(itemDto.getQuantity()));
        amount.add(itemAmount);
        names.add("čarape");
        amounts.add(itemAmount);
        items.add(itemDto);
        OrderRequest request = new OrderRequest();
        request.setCurrency("EUR");
        request.setEmail("igor@samarait.hr");
        request.setItems(items);

        createOrder(request);
    }

    public OrderResponse createOrder(OrderRequest request) {
        log.info("createOrder START {}", request);
        OrderResponse orderResponse = new OrderResponse();
        orderResponse.setId(UUID.randomUUID().toString());
        orderResponse.setCreated(LocalDateTime.now());
        orderResponse.setEmail(request.getEmail());
        BigDecimal amount = BigDecimal.TEN;
        orderResponse.setPrice(new Amount(request.getCurrency(), amount));

        List<OrderResponse.ItemDto> items = new ArrayList<>();
        items.add(new OrderResponse.ItemDto("Lizalica", new BigDecimal("10.11")));
        items.add(new OrderResponse.ItemDto("Bombon", new BigDecimal("4.16")));
        orderResponse.setItems(items);

        orders.add(orderResponse);
        log.info("createOrder END {}", orderResponse);
        return orderResponse;
    }

    public List<OrderResponse> fetchOrders(LocalDateTime startDate, LocalDateTime endDate) {
        log.info("createOrder START {}, {}", startDate, endDate);
        log.info("createOrder END {}", orders);
        return orders;
    }

  public OrderResponse fetchOrder(String id) {
      if (orders.isEmpty())
        return null;
      return orders.get(0);
  }
}
