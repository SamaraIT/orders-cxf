package hr.samarait.demo.orders.client;

import hr.samarait.demo.orders.model.OrderResponse;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;

/**
 * integration test
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Slf4j
public class ClientSpringIT {

  @LocalServerPort
  int randomServerPort;

  @Test
  void fetchOrder() throws Exception {
    RestTemplate restTemplate = new RestTemplate();
    String url = "http://localhost:" + randomServerPort + "/services/demo/orders";

    ResponseEntity<String> response = restTemplate.getForEntity(url + "/1", String.class);
    Assertions.assertEquals(response.getStatusCode(), HttpStatus.OK);
    String xmlResponse = response.getBody();
    log.info("xmlResponse={}", xmlResponse);
    Assertions.assertTrue(xmlResponse.contains("<OrderResponse"));
    Assertions.assertTrue(xmlResponse.contains("<ds:Signature"));

    Unmarshaller unmarshaller = JAXBContext.newInstance(OrderResponse.class).createUnmarshaller();
    OrderResponse orderResponse = (OrderResponse) unmarshaller.unmarshal(new StringReader(xmlResponse));
    log.info("orderResponse={}", orderResponse);
    Assertions.assertNotNull(orderResponse);
  }
}
